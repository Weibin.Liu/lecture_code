package lecture1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSimpleJava {

	@Test
	void testAdd() {
		int sum = SimpleJava.add(5,10);
		assertEquals(15, sum);
	}

}
